``` eval_rst

qplot
=============


**qplot** is a matplotlib based Python library to visualize 
`SciGRID_gas data  <https://www.gas.scigrid.de/downloads.html>`_. 

It can be used in combination with the 
`SciGRID_gas main library <https://www.gas.scigrid.de/downloads.html>`_ or 
the `osmscigrid library <https://www.gas.scigrid.de/downloads.html>`_.


Features
--------

What does it provide:

- A routine to read SciGRID_gas data from CSV files into a SciGRID_gas network class
- Customizable visualization of SciGRID_gas data
- Visualization of parallel pipelines
- A background map for Europe or individual states in Europe
- The thickness and color of pipes can be changed based pipeline attribute values
  (e.g. diameter, pressure or capacity) 
- Provides meta information on mouse click 



Installation
------------

**qplot** depends on a Python (version of 3.6 or above) as well as the
following libraries:

-  matplotlib==3.3.3
-  mplcursors>=0.3
-  PyShp
-  numpy==1.19.4
-  pandas>=0.25.3
-  pathlib>=1.0.1
-  descartes==1.1.0
-  adjustText==0.7.3
-  Unidecode==1.1.1
-  Shapely==1.7.1

Use ``pip`` to install ``qplot``:

.. code:: bash

    $ pip install qplot

Thereafter you can import the quickplot function via:

.. code:: python

    from qplot.qplot import quickplot

Download of WorldBorders
------------------------
To be able to visualize country borders, you need to navigate to

https://thematicmapping.org/downloads/world_borders.php

and download the TM_WORLD_BORDERS-zipfile.
 
You can pass the variable 'TM_Borders_filename'
to the quickplot function. Per default, qplot will expect this file to be parallel to your execution folder of qplot: 

.. code:: python

    TM_Borders_filename='../TM_World_Borders/TM_WORLD_BORDERS-0.3.shp'

License
-------

**qplot** is licensed under the `GNU General Public License version
3.0 <https://www.gnu.org/licenses/gpl-3.0.html>`_.


The Team
--------

**qplot** is developed at the
`DLR <https://www.dlr.de/EN/Home/home_node.html>`_ Institute of
`Networked Energy Systems
<https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-12472/21440_read-49440/>`_
in the department for `Energy Systems Analysis (ESY)
<https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-12471/21741_read-49802/>`_.
```


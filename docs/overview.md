


Quickreference:
====================
The *quickplot* function is the main function of the **qplot** library.
The following list shows the available function options for *quickplot*:

    Parameters
    ----------
    * Netz                instance of a SciGRID_gas network class
    * countrycode         st(2-digit countrycode): used base map [default: 'EU']
    * TM_Borders_filename str, The filepath of you TM_Borders_file [default:
                          '../TM_World_Borders/TM_WORLD_BORDERS-0.3.shp']
    * parameter           str, single parameter from dictionary "param"
                          will be used in conjunction with 
                          "thicklines" 
    * loops               boolean, coloring of parallel pipelines [default: False]

    Overplotting
    ------------
    * Fig                 matplotlib fig-object: return value of quickplot 
    * hold                boolean: true=quickplot will not plot


    Single Style Object
    -------------------
    * alpha               [default: 1],transparency
    * SingleColor         str, same as in Matplotlib [default: '']
    * SingleMarker        str, same as in Matplotlib [default: '']
    * SingleLineWidth     str, same as in Matplotlib [default: '']
    * SingleLabel         str, same as in Matplotlib [default: '']
    * SingleSize          str, same as in Matplotlib [default: '']
    * SingleAlpha         str, same as in Matplotlib [default: '']


    Style Parameters
    ----------------
    * MapColor            boolean, background map in color (True)  [default: True]
    * figureNum           int, number of the figure [default: 1]
    * SupTitleStr         str, of title of plot [default: '']
    * LegendStyle         'Str' or 'Str(Num)': Names or Names + # [default: 'Str']
    * Legend              boolean, shows legend [default=False]
    * Names               boolean, shows component names on map [default: False]
    * Cursor_Points       boolean, mouse click for point components[default: True]
    * Cursor_Lines        boolean, mouse click for line components [default: False]
    * thicklines          boolean, scales pipe thickness with  
                          the option "parameter" [default:False]
    * orig_path           boolean, More details in PipeSegments [default: False]
                           

    Save Figure
    ------------
    * savefile            str, name path [default: '']
    * Save                boolean, to save the plot [default: False]
    * save_dpi            int, dots per inch [default: 300]

    Developer Parameters
    --------------------
    * PlotList            list of components ('Nodes','PipeLines')[Default: ['']
    * IgnoreList          list of components to not plot [Default: ['']]
    * GridOn              boolean, plot a mash grid as background [default: False]
    * Axis                boolean, turn off x and y axis
    * Frame               boolen, turn off outer frame
    * tagstyle            int=1,2,3 or 4, info type on mouse click [default: 1]
    * Info                Info object to customize quickplot [Default: '']







\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Project information}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Background}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}General Information}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}qplot}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Features}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Installation}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Download of WorldBorders}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}License}{6}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}The Team}{6}{subsection.2.1.5}
\contentsline {chapter}{\numberline {3}Quickplot Function}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Quickreference:}{7}{section.3.1}
\contentsline {chapter}{\numberline {4}Usage}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Usage}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Example 1 \sphinxhyphen {} Simple plot}{9}{section.4.2}
\contentsline {section}{\numberline {4.3}Example 2 \sphinxhyphen {} Cursor usage with points}{11}{section.4.3}
\contentsline {section}{\numberline {4.4}Example 3 \sphinxhyphen {} Original pipeline paths}{13}{section.4.4}
\contentsline {section}{\numberline {4.5}Example 4 \sphinxhyphen {} Overplotting}{15}{section.4.5}
\contentsline {section}{\numberline {4.6}Example 5 \sphinxhyphen {} Line thickness and line color}{16}{section.4.6}
\contentsline {section}{\numberline {4.7}Example 6 \sphinxhyphen {} Multi lines}{17}{section.4.7}

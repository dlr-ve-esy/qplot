# Usage

In the following example **qplot** is used to visualize a SciGRID_gas dataset.
The easiest way to run **qplot** is to download the sample.py 
from the [git repository](https://gitlab.com/dlr-ve-esy/qplot) and execute it.
It is quite similar to the more detailed description given below.


Example 1 - Simple plot
=========
In this example, we start by importing the **os** library as well as qplot's *read* 
function from the **qplot** CSV library and the 
*quickplot* function from the **qplot** main libary module. 

```python

>>> import os
>>> from qplot.CSV import read
>>> from qplot.qplot import quickplot

```
Thereafter, the location of the "TM_World_Borders" file is defined.

```python
TM_World_Borders_file='../TM_World_Borders/TM_WORLD_BORDERS-0.3.shp'

```
Subsequently, the SciGRID_gas dataset is read into a SciGRID_gas 
data class. Beside the location of the dataset, we define a String `NameStart`
which is the starting string of each dataset file before the underscore.

The string `NetzName` can later be used for saving the dataset back to a CSV file.
 

```python
INET = read('Data/TestData',NetzName='INET',
           NameStart=('INET'))
```

In the next step we define a `countrycode` of a European country and
reduce the data to this specific country by using the SciGRID_gas
class method *reduce*.

```python
countrycode='LV'
INET_LV=INET.reduce(countrycode)
```

Now, the entire dataset can be visualized with the following command:

```python
quickplot(INET_LV,
          countrycode=countrycode,
          TM_Borders_filename=TM_World_Borders_file)
```
If you have stored your TM_Borders-file accordingly to the manual, 
you can leave the last parameter out, as we will do in the following. 


If you are more interested in specific components of a 
dataset (e.g. `PipeSegments`), you can replace the first parameter 
with a tuple, where first entry is still the
SciGRID_gas netclass object and the following 
entries are the name strings of 
the components of interest.

```python

quickplot((INET_LV,'PipeSegments'),
           countrycode=countrycode)
```
![alt text](./images/LV1.png)

If you want to inspect all available components of  
a SciGRID_gas class object you can use the class method *all*.

```python
INET_LV.all()
```
This will give you the following output:

```
--------------------------------------
BorderPoints                        1
Compressors                         0
ConnectionPoints                    0
Consumers                           0
EntryPoints                         1
InterConnectionPoints               1
LNGs                                1
Nodes                              13
PipeLines                           0
PipePoints                          0
PipeSegments                       12
Processes                           1
Productions                         0
Storages                            1
--------------------------------------
Length of PipeLines    [km]         0
Length of PipeSegments [km]       867
```

Generally, SciGRID gas considers 'PipeLines' and 'PipeSegments' as line components and the others as point components,
which results in partially different visualization options.


Example 2 - Cursor usage with points
=========
In this example it is shown how you inspect meta data of 
point components on the example of `LNGs`.


```python

quickplot((INET_LV,'PipeSegments','LNGs'),
          countrycode=countrycode,
          Cursor_Points=True)
```
![alt text](./images/LV4.png)

Since the option `Cursor_Points` is set per default to "True", we could leave it out.
In order to check for available component parameters, we can
use the *all* method.
In this example we list all parameters of the first LNG terminal in the dataset.

```python
INET_LV.LNGs[0].all()
```
You will get the following:

```
comment: None
country_code: LV
id: INET_LG_0
lat: 56.9488
long: 24.1063
method: 
     ('max_cap_store2pipe_M_m3_per_d', 'raw')
     ('max_workingGas_M_m3', 'raw')
     ('end_year', 'make_Attrib(const)')
     ('start_year', 'make_Attrib(const)')
     ('is_H_gas', 'make_Attrib(const)')
name: Riga
node_id: ['INET_N_602']
param: 
     ('max_cap_store2pipe_M_m3_per_d', 16.71232876712329)
     ('max_workingGas_M_m3', 110.92671200000001)
     ('end_year', 2050)
     ('start_year', 1983)
     ('is_H_gas', 1)
source_id: ['INET_LG_0']
tags: 
uncertainty: 
     ('max_cap_store2pipe_M_m3_per_d', 0)
     ('max_workingGas_M_m3', 0)
     ('end_year', 20)
     ('start_year', 20)
     ('is_H_gas', 1)
```
Meta parameters are stored in a dictionary called `param`.
You can access any of those parameters in `param` via `parameter` in quickplot.
(You could also access one of the other three dictionaries (`method`, `uncertainty`,`tags`)
with this method. For example, to visualize a parameter in `method` just add the option
`dictionary` = "method" in quickplot.) 

We demonstrate the visualisation of the parameter *start_year* for LNG terminals in Latvia.

```python

quickplot((INET_LV,'PipeSegments','LNGs'),
          countrycode=countrycode,
          parameter='start_year')
```

As you see, there is currently a single LNG terminal in Latvia.
By click on the terminal additional information will now be visible 
including the non-standart information `start_year`.

![alt text](./images/LV0.png)

But how do you get information about line components like `PipeSegments`?

1. Click on a line element and receive information in your console (always enabled)
2. You can add `Cursor_Lines`=True (Example 4)



Example 3 - Original pipeline paths
=========
Some SciGRID_gas datasets (e.g. OSM, EMap) have additional intermediate `lat`/`long` values
values stored in their Component `PipeSegments`. They are located 
in `param` under `path_lat`/`path_long`. They can be used for visualization.   
In this example, we load a SciGRID_gas OSM-dataset, which has been originally created 
with the python library **osmscigrid**.

```python
OSM = read('Data/TestData',NetzName='OSM',
           NameStart=('OSM'))
```

This dataset is already reduced to Latvia for user convinience.
You can visually inspect `PipeSegments` with:

```python
quickplot((OSM,'PipeSegments'),
          countrycode=countrycode)
```
![alt text](./images/LV1.png)

*OSM dataset for Latvia visualized with the option* "`orig_path`=False"*.*

Now we set `orig_path` = True.

```python
quickplot((OSM,'PipeSegments'),
          countrycode=countrycode
          orig_path=True)
```
![alt text](./images/LV2.png)

*OSM dataset for Latvia visualized with the option* "`orig_path`=True"*.*

The difference between both cases depends on the underlying datasets.

Example 4 - Overplotting
=========

In this example we plot `PipeSegments` from both, the OSM and the INET dataset simultaneously.
For this purpose, we will call quickplot for both datasets.
However, we can suppress plotting the first dataset with the `hold` option.
Now, we pass the return value of the first plot to the `Fig` option of the next *quickplot* call, this time 
without the `hold` option. 


```python
fig1=quickplot((OSM,"PipeSegments"),
           Cursor_Lines=True,
           Cursor_Points=False,
           orig_path=True,
           SingleColor='red',
           SingleLineStyle='solid',
           hold=True,
           countrycode=countrycode)

quickplot((INET_LV,"PipeSegments"),
           Fig=fig1,
           Cursor_Lines=True,
           Cursor_Points=False,
           countrycode=countrycode)
```

![alt text](./images/LV3.png)
    
In this example we have further used `SingleColor` and `SingleLineStyle` 
to make both dataset distinguishable. Additionally, we have omitted information on mouse click for point components with the 
option `Cursor_Points` and further activated it for line components with the option `Cursor_Lines` = True.

Example 5 - Line thickness and line color 
=========

In this example we demonstrate how line components can be visualized
with the variables `thicklines` and `parameter`.


```python
quickplot((INET),
           parameter='diameter_mm',
           # or:  max_pressure_bar
           # or   max_cap_M_m3_per_d
           thicklines=True,
           countrycode='EU',
           Legend=True)
```

![alt text](./images/EU.png)

In the next image we have visualized the `PipeSegments` with `max_pressure_bar`
instead of `diameter_mm`. 

```python
quickplot((INET),
           parameter='max_pressure_bar',
           thicklines=True,
           countrycode='EU',
           Legend=True,
           LegendStyle='Str(Num)')
```
![alt text](./images/EU2.png)
    
Further we demonstrate the use of `Legend` and `LegendStyle`. The latter can be set to "Str(Num)" to count all elements per component.

Example 6 - Multi lines 
=========

In this example we demonstrate how to visualise multiple parallel 
pipelines with the `loops` parameter.

```
quickplot((INET2,"PipeSegments"),
           loops=True,
           Legend=False,
           countrycode="IT",
           TM_Borders_filename=TM_World_Borders_file)
```
![alt text](./images/IT.png)

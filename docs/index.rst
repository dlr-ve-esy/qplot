Welcome to qplot's documentation!
---------------------------------


*How to cite*

| A. Pluta, J.C.Diettrich, W. Medjroubi
| SciGRID_gas: qplot
| DLR Institute for Networked Energy Systems
| Germany


*Impressum*

| DLR Institute for Networked Energy Systems
| Carl-von-Ossietzky-Str. 15
| 26129 Oldenburg
| Germany
| Tel.: +49 (441) 999 060



.. _ChapterIntroduction:

*******************
Introduction
*******************

.. toctree::
    :maxdepth: 2
    :includehidden:
    :glob:
    :caption: Introduction

    intro   




.. _ChapterInformation:

*******************
General Information
*******************

.. toctree::
    :maxdepth: 2
    :includehidden:
    :glob:
    :caption: Contents

    about   



.. _ChapterDataStructure:

******************
Quickplot Function
******************

.. toctree::
    :maxdepth: 2
    :includehidden:
    :glob:
    :caption: Overview

    overview

**************
Usage
**************

.. toctree::
    :maxdepth: 2
    :includehidden:
    :glob:
    :caption: Usage

    usage


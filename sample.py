import os
from qplot.CSV import read
from qplot.qplot import quickplot

countrycode='EU'

#TM_World_Borders_file='../TM_World_Borders/TM_WORLD_BORDERS-0.3.shp'
INET = read('Data/TestData',NetzName='INET',
           NameStart=('INET'))

INET2=INET.reduce(countrycode)



    

quickplot((INET2,"PipeSegments"),
           Cursor_Lines=True,
           Cursor_Points=True,
           thicklines=True,
           #loops=True,
           #parameter='diameter_mm',
           parameter='max_pressure_bar',
           #parameter='start_year',
           Legend=False,
           LegendStyle='Str(Num)',
           countrycode=countrycode)



